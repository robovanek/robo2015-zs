package TuxTARDIS.Rewrite.VM;

import TuxTARDIS.Rewrite.Const;
import TuxTARDIS.Rewrite.Navigation.*;
import TuxTARDIS.Rewrite.Utils.CMD;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;

/**
 * Instruction packer based on Follower/Scheduler
 */
public class Packer implements Runnable {
	private final Queue<CMD> commands = new LinkedList<>();
	private final Map map;
	private final BlockingQueue<Instruction> output;
	private final Queue<Pos> path;

	public Packer(Map map, Path path, BlockingQueue<Instruction> instructions) {
		this.map = map;
		this.path = path.path;
		this.output = instructions;
	}

	@Override
	public void run() {
		output.add(new ConfInstruction(ConfInstruction.SPEED, Const.SPEED));
		output.add(new ConfInstruction(ConfInstruction.ACCEL, Const.ACCEL));
		while (path.size() > 0) {
			Pos pozice = path.poll(); // vezmi souřadnice další pozice
			if (pozice == map.curPos) // pokud už na této pozici jsme
				continue; // přeskoč
			int direction = Pos.directionOf(map.curPos, pozice); // zjisti směr jízdy
			int difference = MapOrient.difference(map.curOrient, direction); // zjisti odklonění od současného směru
			if (difference == 0) // pokud máme jet rovně
				forward(); // jeď rovně

			else if (difference == 2) // pokud máme jet dozadu
				backward(); // jeď dozadu

			else if (difference == -1) { // pokud máme jet doleva
				left(); // zahni doleva
				forward(); // jeď rovně
			} else if (difference == 1) { // pokud máme jet doprava
				right(); // zahni doprava
				forward(); // jeď rovně
			}
		}
		sendEnd(); // dodělej cache, v Main.main() je projistotu znovu
	}

	/**
	 * Přidá příkaz pro jízdu dopředu
	 */
	private void forward() {
		commands.add(CMD.Forward); // přidá do fronty
		map.travel(1, false); // popojede v mapě
		if (need_flush())
			process_queue();
	}

	/**
	 * Přidá příkaz pro jízdu do
	 */
	private void backward() {
		commands.add(CMD.Backward); // přidá do fronty
		map.travel(1, true); // popojede v mapě
		if (need_flush())
			process_queue();
	}

	/**
	 * Přidá příkaz pro jízdu do
	 */
	private void left() {
		commands.add(CMD.ToLeft); // přidá do fronty
		map.turn(CMD.ToLeft, 1); // pootočí se v mapě
		if (need_flush())
			process_queue();
	}

	/**
	 * Přidá příkaz pro jízdu do
	 */
	private void right() {
		commands.add(CMD.ToRight); // přidá do fronty
		map.turn(CMD.ToRight, 1); // pootočí se v mapě
		if (need_flush())
			process_queue();
	}

	/**
	 * Zjistí, jestli jsou ve frontě jen CMD.Forward a CMD.Backward
	 *
	 * @return Nález
	 */
	private boolean need_flush() {
		if (commands.size() == 0) // pokud nic nemáme
			return true; // ano OK
		Queue<CMD> temp = new LinkedList<>(commands); // zkopírujeme frontu
		while (temp.size() > 0) { // dokud jí nevyprázdníme
			CMD test = temp.poll(); // vezmi si příkaz
			if (test != CMD.Backward && test != CMD.Forward) // pokud to není dopředu/dozadu
				return true; // ne, fronta není homogenní
		}
		return false; // ano, fronta je homogenní
	}

	/**
	 * Vykoná cache
	 */
	private void process_queue() {
		while (commands.size() > 0) { // dokud nevyčerpáme příkazy
			CMD task = commands.peek(); // nakoukni na příkaz
			int count = count_elements(); // spočítej počet příkazů

			int type = MoveInstruction.FORWARD;
			switch (task) {
				case Forward:
					if (test_fwd_align())
						type = MoveInstruction.FRONT_ALIGN;
					else
						type = MoveInstruction.FORWARD;
					break;
				case Backward:
					type = MoveInstruction.BACKWARD;
					break;
				case ToLeft:
					type = MoveInstruction.TURN_LEFT;
					break;
				case ToRight:
					type = MoveInstruction.TURN_RIGHT;
					break;
			}
			output.add(new MoveInstruction(type, count, null, 0, null));
		}
	}

	/**
	 * Zjistli, jestli lze narazit na zeď před robotem, pokud ano, srovnej robota
	 */
	private boolean test_fwd_align() {
		CMD[] prikazy = commands.toArray(new CMD[commands.size()]); // zkopírujeme frontu
		int curOrient = map.curOrient; // zkopírujeme současný směr
		Pos curPos = map.curPos; // zkopírujeme současnou pozici
		for (int i = prikazy.length - 1; i >= 0; i--) { // postupně se skrz frontu prokousej na reálnou současnou pozici
			switch (prikazy[i]) {
				case Forward: // pokud bychom jeli dopředu
					curPos = Pos.modify(curPos, curOrient, -1); // dozadu
					break;
				case Backward: // pokud bychom jeli dozadu
					curPos = Pos.modify(curPos, curOrient, 1);// dopředu
					break;
				case ToLeft: // pokud bychom zatáčeli doleva
					curOrient = MapOrient.edit(curOrient, 1);// doprava
					break;
				case ToRight: // pokud bychom zatáčeli doprava
					curOrient = MapOrient.edit(curOrient, -1);// doleva
					break;
			}
		}
		Pos node = Pos.modify(curPos, curOrient, 1);
		return map.get(node) == TileType.Wall;
	}

	/**
	 * Spočítá počet příkazů, které také odebere.
	 *
	 * @return Počet stejných příkazů
	 */
	private int count_elements() {
		CMD first = commands.poll(); // získá první příkaz
		if (first == null) // pokud žádný není
			return 0; // vrať 0

		int count = 1; // počet je min. jeden - už jsme jeden vzali
		while (commands.size() > 0) { // dokud nedojdou příkazy
			if (commands.peek() != first) // pokud se další neshoduje s prvním
				break; // stop
			count++; // zvyš čítač
			commands.poll(); // seber ten, na který jsme nakoukli přes peek()
		}
		return count; // vrať počet
	}

	/**
	 * Dokončí práci z fronty
	 */
	private void sendEnd() {
		while (commands.size() > 0) // dokud máme příkazy
			process_queue(); // vykonávej
		output.add(new ExitInstruction());
	}


}
