package TuxTARDIS.Rewrite.VM;

/**
 * Exit instruction
 */
public class ExitInstruction implements Instruction {
	public ExitInstruction(){}
	public int getType(){
		return 0;
	}
}
