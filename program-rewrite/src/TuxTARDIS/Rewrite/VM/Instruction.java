package TuxTARDIS.Rewrite.VM;

/**
 * Instruction base
 */
public interface Instruction {
	int getType();
}
