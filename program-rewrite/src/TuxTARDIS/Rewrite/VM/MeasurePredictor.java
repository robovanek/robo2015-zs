package TuxTARDIS.Rewrite.VM;

import TuxTARDIS.Rewrite.Const;
import TuxTARDIS.Rewrite.Navigation.Map;
import TuxTARDIS.Rewrite.Navigation.MapOrient;
import TuxTARDIS.Rewrite.Navigation.MeasurementSet;
import TuxTARDIS.Rewrite.Utils.CMD;
import TuxTARDIS.Rewrite.Utils.Common;
import TuxTARDIS.Rewrite.Utils.FloatRange;

import java.util.concurrent.BlockingQueue;

/**
 * Predicts sensor measurements and embeds these predictions into instruction stream.
 */
public class MeasurePredictor implements Runnable {
	private final Map map;
	private final BlockingQueue<Instruction> input;
	private final BlockingQueue<Instruction> output;

	public MeasurePredictor(Map map, BlockingQueue<Instruction> input, BlockingQueue<Instruction> output) {
		this.map = map;
		this.input = input;
		this.output = output;
	}

	@Override
	public void run() {
		boolean exit = false;
		while (!exit) {
			Instruction op = Common.getNext(input);

			if (op instanceof ExitInstruction) {
				exit = true;
			} else if (op instanceof MoveInstruction) {
				MoveInstruction move = (MoveInstruction) op;
				move.setStartPos(map.curPos);
				move.setStartOrient(map.curOrient);

				switch (move.getType()) {
					case MoveInstruction.FORWARD:
						map.travel(move.getCount(), false);
						break;
					case MoveInstruction.BACKWARD:
						map.travel(move.getCount(), true);
						break;
					case MoveInstruction.FRONT_ALIGN:
						map.travel(move.getCount(), false);
						break;
					case MoveInstruction.BACK_ALIGN:
						map.travel(map.beforeWall(
								MapOrient.edit(map.curOrient, 2),
								map.curPos), true);
						break;
					case MoveInstruction.TURN_RIGHT:
						map.turn(CMD.ToRight, move.getCount());
						break;
					case MoveInstruction.TURN_LEFT:
						map.turn(CMD.ToLeft, move.getCount());
						break;
				}
				FloatRange north = new FloatRange(map.beforeWall(
						MapOrient.North, map.curPos) * Const.TILE_LENGTH + Const.SONIC_SPACING, Const.THRESHOLD);
				FloatRange east = new FloatRange(map.beforeWall(
						MapOrient.East, map.curPos) * Const.TILE_LENGTH + Const.SONIC_SPACING, Const.THRESHOLD);
				FloatRange south = new FloatRange(map.beforeWall(
						MapOrient.South, map.curPos) * Const.TILE_LENGTH + Const.SONIC_SPACING, Const.THRESHOLD);
				FloatRange west = new FloatRange(map.beforeWall(
						MapOrient.West, map.curPos) * Const.TILE_LENGTH + Const.SONIC_SPACING, Const.THRESHOLD);
				MeasurementSet set = new MeasurementSet(new FloatRange[]{north, east, south, west});
				move.setMeasurements(set);
				op = move;
			}
			output.add(op);
		}
	}
}