package TuxTARDIS.Rewrite.VM;

/**
 * Configuration instruction
 */
public class ConfInstruction implements Instruction {
	public static final int SPEED = 0;
	public static final int ACCEL = 1;
	private final int type;
	private final int value;
	public ConfInstruction(int type, int value) {
		this.type = type;
		this.value = value;
	}

	@Override
	public int getType() {
		return type;
	}

	public int getValue() {
		return value;
	}
}
