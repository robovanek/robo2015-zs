package TuxTARDIS.Rewrite.VM;

import TuxTARDIS.Rewrite.Navigation.MapOrient;
import TuxTARDIS.Rewrite.Navigation.MeasurementSet;
import TuxTARDIS.Rewrite.Navigation.Pos;
import TuxTARDIS.Rewrite.Utils.FloatRange;

/**
 * Move instruction container
 */
public class MoveInstruction implements Instruction {
	public static final int FORWARD=0;
	public static final int BACKWARD=1;
	public static final int TURN_LEFT=2;
	public static final int TURN_RIGHT=3;
	public static final int BACK_ALIGN=4;
	public static final int FRONT_ALIGN=5;

	public Pos getStartPos() {
		return startPos;
	}

	public void setStartPos(Pos startPos) {
		this.startPos = startPos;
	}

	public int getStartOrient() {
		return startOrient;
	}

	public void setStartOrient(int startOrient) {
		this.startOrient = startOrient;
	}

	@Override
	public int getType() {
		return type;
	}

	public int getCount() {
		return count;
	}

	public MeasurementSet getMeasurements() {
		return measurements;
	}

	public void setMeasurements(MeasurementSet measurements) {
		this.measurements = measurements;
	}
	public FloatRange[] getExpectedSonic(){
		return measurements.getExpectedSonic();
	}
	public int getEndOrient() {
		if (type == TURN_LEFT)
			return MapOrient.edit(startOrient, -count);
		if (type == TURN_RIGHT)
			return MapOrient.edit(startOrient, count);
		return startOrient;
	}
	private Pos startPos;
	private int startOrient;
	private final int type;
	private final int count;
	private MeasurementSet measurements;

	public MoveInstruction(int type, int count, Pos startPos, int startOrient, MeasurementSet measurements){
		this.type = type;
		this.count = count;
		this.startPos = startPos;
		this.startOrient = startOrient;
		this.measurements = measurements;
	}
}
