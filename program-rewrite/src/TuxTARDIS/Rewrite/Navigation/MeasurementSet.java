package TuxTARDIS.Rewrite.Navigation;

import TuxTARDIS.Rewrite.Utils.FloatRange;

/**
 * Set of measurements of physical quantities (distance)
 */
public class MeasurementSet {

	public FloatRange[] getExpectedSonic() {
		return expectedSonic;
	}

	public void setExpectedSonic(FloatRange[] expectedSonic) {
		this.expectedSonic = expectedSonic;
	}

	private FloatRange[] expectedSonic;

	public MeasurementSet(FloatRange[] expectedSonic) {
		this.expectedSonic = expectedSonic;
	}
	public FloatRange[] createRange(FloatRange north, FloatRange east, FloatRange south, FloatRange west){
		FloatRange[] data = new FloatRange[4];
		data[MapOrient.North].set(north);
		data[MapOrient.East].set(east);
		data[MapOrient.South].set(south);
		data[MapOrient.West].set(west);
		return data;
	}
	public FloatRange[] createRange(int northMin,int northMax,
	                              int eastMin,int eastMax,
	                              int southMin,int southMax,
	                              int westMin,int westMax){
		FloatRange[] data = new FloatRange[4];
		data[MapOrient.North].set(northMin,northMax);
		data[MapOrient.East].set(eastMin,eastMax);
		data[MapOrient.South].set(southMin,southMax);
		data[MapOrient.West].set(westMin,westMax);
		return data;
	}
}
