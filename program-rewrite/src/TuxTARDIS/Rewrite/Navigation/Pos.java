package TuxTARDIS.Rewrite.Navigation;

/**
 * "Kontejner" pro souřadnice; bod
 * @author Jakub Vaněk
 */
public class Pos {
	/**
	 * X-ová souřadnice
	 */
	public final int x;
	/**
	 * Y-ová souřadnice
	 */
	public final int y;
	/**
	 * Neplatná pozice, také "nic"
	 */
	public static final Pos none = null;


	/**
	 * Konstruktor
	 *
	 * @param x X-ová souřadnice
	 * @param y Y-ová souřadnice
	 */
	public Pos(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Posune bod v daném směru o daný počet
	 *
	 * @param orig   Původní pozice
	 * @param orient Směr posunu
	 * @param count  Velikost posunu
	 * @return Posunutý bod
	 */
	public static Pos modify(Pos orig, int orient, int count) {
		switch (orient) // větvení na základě směru
		{
			case TuxTARDIS.Rewrite.Navigation.MapOrient.North: // nahoru
				return new Pos(orig.x, orig.y - count); // sniž Y o daný počet
			case TuxTARDIS.Rewrite.Navigation.MapOrient.East: // doprava
				return new Pos(orig.x + count, orig.y); // zvyš X o daný počet
			case TuxTARDIS.Rewrite.Navigation.MapOrient.South: // dolů
				return new Pos(orig.x, orig.y + count); // zvyš Y o daný počet
			case TuxTARDIS.Rewrite.Navigation.MapOrient.West: // doleva
				return new Pos(orig.x - count, orig.y); // sniž X o daný počet
			default: // neplatný směr
				return orig; // žádný posun
		}
	}

	/**
	 * Získá sousední body
	 *
	 * @param p Základový bod
	 * @return Pole sousedů
	 */
	public static Pos[] adjacent(Pos p) {
		return new Pos[]{new Pos(p.x, p.y - 1), // nahoru
				new Pos(p.x + 1, p.y), // doprava
				new Pos(p.x, p.y + 1), // dolů
				new Pos(p.x - 1, p.y)}; // doleva
	}

	/**
	 * Vrátí optimalizovaný seznam okolních bodů
	 *
	 * @param curPos    Současná pozice
	 * @param curOrient Současný směr
	 * @return Okolní pozice
	 */
	public static Pos[] adjacent(Pos curPos, int curOrient) {
		return new Pos[]{
				Pos.modify(curPos, curOrient, 1), // přední
				Pos.modify(curPos, curOrient, -1), // zadní
				Pos.modify(curPos, MapOrient.edit(curOrient, -1), 1), // vlevo
				Pos.modify(curPos, MapOrient.edit(curOrient, 1), 1) // vpravo
		};
	}

	/**
	 * Získá směr daného bodu vzhledem k původnímu bodu
	 *
	 * @param original Původní bod (odkud)
	 * @param next     Nový bod (kam)
	 * @return Směr
	 * @see MapOrient
	 */
	public static int directionOf(Pos original, Pos next) {
		if (original.x == next.x && original.y > next.y) // úbytek Y, nahoru
			return MapOrient.North;
		if (original.x < next.x && original.y == next.y) // příbytek X, doprava
			return MapOrient.East;
		if (original.x == next.x && original.y < next.y) // příbytek Y, dolů
			return MapOrient.South;
		if (original.x > next.x && original.y == next.y) // úbytek X, doleva
			return MapOrient.West;
		return 0;
	}

	/**
	 * Převede bod na řetězec (x,y)
	 */
	@Override
	public String toString() {
		return Integer.toString(x) + "," + Integer.toString(y); // formát a nasunutí čísel
	}

	/**
	 * Ověří shodnost objektu
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Pos) // pokud objekt jsou souřadnice
		{
			Pos p = (Pos) obj;
			if (p.x == this.x && p.y == this.y) // pokud jsou X i Y schodné
				return true; // ano, objekty jsou shodné
		}
		return false; // objekty nejsou shodné
	}

	/**
	 * Experimentální funkce pro unikátní převod souřadnic na číslo
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}
}
