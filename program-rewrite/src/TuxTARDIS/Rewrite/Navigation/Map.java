package TuxTARDIS.Rewrite.Navigation;

import TuxTARDIS.Rewrite.Utils.CMD;

import java.util.*;

/**
 * Virtuální mapa
 * Pozn. blok = pole
 */
public class Map {
	// PROMĚNNÉ A KONSTANTY
	/**
	 * X-ová velikost mapy (svislá)
	 */
	public static final int sizeX = 11;
	/**
	 * Y-ová velikost mapy (vodorovná)
	 */
	public static final int sizeY = 8;
	/**
	 * Pozice středu mapy (startu)
	 */
	public static final Pos center = new Pos(5, 4);
	/**
	 * Interní reprezentace
	 */
	private final int[][] map;
	/**
	 * Současná orientace robota; start je nahoru
	 */
	public int curOrient = MapOrient.North;

	/**
	 * Současná pozice robota; robot startuje na startu ;)
	 */
	public Pos curPos = new Pos(center.x, center.y);


	// FUNKCE

	/**
	 * Konstruktor
	 */
	public Map() {
		// Vytvořit mapu
		map = new int[sizeX][sizeY];
		// Naplnit mapu neznámými bloky
		for (int[] part : map)
			Arrays.fill(part, TileType.LightOn);
		// Nakreslit okraje
		for (int x = 0; x < sizeX; x++) { // vodorovně
			set(x, 0, TileType.Wall); // nahoře
			set(x, sizeY - 1, TileType.Wall); // dole
		}
		for (int y = 1; y < sizeY - 1; y++) { // svisle
			set(0, y, TileType.Wall); // vlevo
			set(sizeX - 1, y, TileType.Wall); // vpravo
		}
		// start a bloky kolem cíle jsou také překážky
		set(center.x - 1, center.y, TileType.Wall);
		set(center.x, center.y, TileType.Wall);
		set(center.x + 1, center.y, TileType.Wall);
	}
	public Map(Map copy){
		map = new int[sizeX][sizeY];
		for(int x = 0; x < sizeX;x++){
			System.arraycopy(copy.map[x],0,this.map[x],0,sizeY);
		}
		curOrient = copy.curOrient;
		curPos = copy.curPos;
	}

	/**
	 * Načte mapu z poskytnutého obsahu
	 *
	 * @param contents Obsah mapy
	 */
	public Map(String contents) {
		this(); // inicializuje základ
		this.load(contents); // načte
	}

	/**
	 * Získá pole z mapy
	 *
	 * @param p Pozice
	 * @return Pole
	 */
	public int get(Pos p) {
		return map[p.x][p.y];
	}

	/**
	 * Získá pole z mapy
	 *
	 * @param x Souřadnice X
	 * @param y Souřadnice Y
	 * @return Pole
	 */
	public int get(int x, int y) {
		return map[x][y];
	}

	/**
	 * Nastaví pole v mapě
	 *
	 * @param x     Souřadnice X
	 * @param y     Souřadnice Y
	 * @param value Pole
	 */
	public void set(int x, int y, int value) {
		map[x][y] = value;
	}
	public void set(Pos pos, int value) {
		map[pos.x][pos.y] = value;
	}

	/**
	 * Načte mapu z textového obsahu
	 *
	 * @param data Data mapy
	 */
	public void load(String data) {
		data = data.replaceAll("\\s", ""); // odstranění mezer
		data = data.replaceAll("\\n", ""); // odstranění nových řádků
		String[] pozice = data.split(","); // seznam polí
		int index = 0; // iterace
		for (int y = 0; y < sizeY; y++) { // procházej po sloupcích
			for (int x = 0; x < sizeX; x++) { // v každém sloupci procházej po řádcích
				if (pozice[index].equals("X")) // pokud je na daném místě X - zeď
					set(x, y, TileType.Wall); // nastav zeď
				else // blok je volné místo
					set(x, y, TileType.LightOn); // nastav volné místo
				index++; // posuň se
			}
		}
	}

	/**
	 * Zatočí v mapě
	 *
	 * @param where Kam se má točit
	 * @param count Kolik 90° otočení se má udělat
	 */
	public void turn(CMD where, int count) {
		if (where == CMD.ToLeft) // pokud netočíme doprava
			count *= -1; // udělej počet záporný
		curOrient = MapOrient.edit(curOrient, count); // otoč směr
	}

	/**
	 * Jede přímo dopředu/dozadu o daný počet polí; započítává projetá pole
	 *
	 * @param count    Count of tiles to travel
	 * @param backward Whether to go backward or not
	 */
	public void travel(int count, boolean backward) {
		// orientace
		int myOrient = curOrient;
		// zde byl bug, pokud jedeme dozadu, "otočit se"
		if (backward)
			myOrient = MapOrient.edit(curOrient, 2);
		switch (myOrient) { // větvení ze směru
			case MapOrient.North:
				for (int y = curPos.y, i = 0; i <= count; i++, y--) // úbytek Y
					setSeen(curPos.x, y); // nastav celou cestu na LightOff
				break;
			case MapOrient.East:
				for (int x = curPos.x, i = 0; i <= count; i++, x++) // příbytek X
					setSeen(x, curPos.y); // nastav celou cestu na LightOff
				break;
			case MapOrient.South:
				for (int y = curPos.y, i = 0; i <= count; i++, y++) // příbytek Y
					setSeen(curPos.x, y); // nastav celou cestu na LightOff
				break;
			case MapOrient.West:
				for (int x = curPos.x, i = 0; i <= count; i++, x--) // úbytek X
					setSeen(x, curPos.y); // nastav celou cestu na LightOff
				break;
		}
		curPos = Pos.modify(curPos, myOrient, count); // upraví samotnou pozici robota
	}

	/**
	 * Nastaví pole jako již navštívené (tzn. pole = TileType.LightOff)
	 *
	 * @param x Souřadnice X
	 * @param y Souřadnice Y
	 */
	public void setSeen(int x, int y) {
		if (get(x, y) == TileType.LightOn) { // pokud je pole rozsvícené světlo
			set(x, y, TileType.LightOff);
		}
	}

	public int beforeWall(int orient, Pos pos){
		int counter = 0;
		Pos now = Pos.modify(pos,orient,1);
		int tile = this.get(now);
		while(tile!=TileType.Wall) {
			now=Pos.modify(now,orient,1);
			tile = this.get(now);
			counter++;
		}
		return counter;
	}
	public int beforeWall(){
		return beforeWall(curOrient,curPos);
	}
}
