package TuxTARDIS.Rewrite;

import java.util.HashMap;

/**
 * Exported constants
 */
public class Const {
	public static float SONIC_SPACING;
	public static int THRESHOLD;
	// MAIN
	/**
	 * Port tlačítka
	 */
	public static String TOUCH_PORT_FRONT;
	public static String SONIC_PORT;
	/**
	 * Celkový počet map
	 */
	public static int MAPS;
	/**
	 * Rychlost robota v procentech z maximální rychlosti
	 */
	public static int SPEED;
	/**
	 * Složka, ve které jsou mapy, bez koncového lomítka
	 */
	public static String MAP_DIR;

	// MECHANICS
	/**
	 * Průměr kol
	 */
	public static float DIAMETER;
	/**
	 * Vzdálenost mezi koly (pozor, adjusted)
	 */
	public static float TRACKWIDTH;
	/**
	 * Maximální tolerovaná odchylka při otáčení
	 */
	public static int MAXERROR;
	/**
	 * Multiplikátor oprav
	 */
	public static float TURN_KP;
	/**
	 * Zrychlení motorů ve °/(s*s)
	 */
	public static int ACCEL;
	/**
	 * Port levého motoru
	 */
	public static String LEFT_PORT;
	/**
	 * Port pravého motoru
	 */
	public static String RIGHT_PORT;
	/**
	 * Port gyroskopu
	 */
	public static String GYRO_PORT;

	// SCHEDULER
	/**
	 * Délka pole v centimetrech
	 */
	public static int TILE_LENGTH;
	/**
	 * Popojetí zpět při zarovnávání
	 */
	public static int TRAVELBACK;
	/**
	 * Popojetí zpět při zarovnávání (milisekundy)
	 */
	public static int TRAVELBACK_DELAY;

	// MAPSELECTOR
	/**
	 * Prodleva mezi zjišťováním stisknutí tlačítek (milisekundy)
	 */
	public static int MAPSELECT_DELAY;

	// DYNAMICKÝ CONST
	/**
	 * Konfigurační soubor
	 */
	public static String CONFIG = "/home/lejos/programs/tux.conf";
	/**
	 * Načte hodnoty ze souboru
	 */
	public static void load(){
		String data = Main.load_txt(CONFIG); // načte konfigurák
		data = data.replaceAll("\\s", ""); // odstranění mezer
		data = data.replaceAll("\\n", ""); // odstranění nových řádků
		String[] settings = data.split(";"); // rozdělí na jednotlivá nastavení
		HashMap<String,String> map = new HashMap<>(settings.length); // slovník key -> value
		for(String keypair:settings){ // projde nastavení
			String[] separated = keypair.split("="); // rozdělí na key a value
			map.put(separated[0],separated[1]); // zapíše do slovníku
		}
		apply(map); // předá nastavení ze slovníku do hodnot této třídy
	}

	/**
	 * Načte hodnoty konstant ze slovníku
	 * @param settings Slovník nastavení
	 */
	private static void apply(HashMap<String,String> settings){
		// String
		MAP_DIR = settings.get("MAP_DIR");
		// Ports
		GYRO_PORT = settings.get("GYRO_PORT");
		RIGHT_PORT = settings.get("RIGHT_PORT");
		LEFT_PORT = settings.get("LEFT_PORT");
		TOUCH_PORT_FRONT = settings.get("TOUCH_PORT_FRONT");
		SONIC_PORT = settings.get("SONIC_PORT");
		// Integer
		MAPS = Integer.parseInt(settings.get("MAPS"));
		SPEED = Integer.parseInt(settings.get("SPEED"));
		ACCEL = Integer.parseInt(settings.get("ACCEL"));
		MAXERROR = Integer.parseInt(settings.get("MAXERROR"));
		TILE_LENGTH = Integer.parseInt(settings.get("TILE_LENGTH"));
		TRAVELBACK = Integer.parseInt(settings.get("TRAVELBACK"));
		TRAVELBACK_DELAY = Integer.parseInt(settings.get("TRAVELBACK_DELAY"));
		MAPSELECT_DELAY = Integer.parseInt(settings.get("MAPSELECT_DELAY"));
		THRESHOLD = Integer.parseInt(settings.get("THRESHOLD"));

		// Float
		DIAMETER = Float.parseFloat(settings.get("DIAMETER"));
		TRACKWIDTH = Float.parseFloat(settings.get("TRACKWIDTH"));
		TURN_KP = Float.parseFloat(settings.get("TURN_KP"));
		SONIC_SPACING = Float.parseFloat(settings.get("SONIC_SPACING"));
	}
}
