package TuxTARDIS.Rewrite.Mechanics;

import lejos.hardware.sensor.EV3GyroSensor;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.robotics.RegulatedMotor;

/**
 * Base pilot interface for CPU
 */
public interface TuxPilot {
	void travel(double distance);
	void stop();
	void rotate(double angle);
	void alignBack();
	void alignFront();
	void forward();
	void backward();
	void setTravelSpeed(double speed);
	void setRotateSpeed(double speed);
	void setAcceleration(int accel);


	class PilotProperties {
		private final int trackWidth, wheelDiameter;
		private final RegulatedMotor left, right;
		private final EV3TouchSensor back, front;
		private final EV3GyroSensor gyro;
		private final double travelback;

		public PilotProperties(float trackWidth, float wheelDiameter,
		                       RegulatedMotor right, RegulatedMotor left,
		                       EV3TouchSensor touchFront, EV3TouchSensor touchRear,
		                       EV3GyroSensor gyro, double travelback){
			this.trackWidth = Math.round(trackWidth);
			this.wheelDiameter = Math.round(wheelDiameter);
			this.right = right;
			this.left = left;
			this.back = touchRear;
			this.front = touchFront;
			this.travelback = travelback;
			this.gyro = gyro;
		}

		public int getTrackWidth() {
			return trackWidth;
		}

		public int getWheelDiameter() {
			return wheelDiameter;
		}

		public RegulatedMotor getLeftMotor() {
			return left;
		}

		public RegulatedMotor getRightMotor() {
			return right;
		}

		public EV3TouchSensor getBackTouch() {
			return back;
		}

		public EV3TouchSensor getFrontTouch() {
			return front;
		}

		public EV3GyroSensor getGyro() {
			return gyro;
		}

		public double getTravelback() {
			return travelback;
		}
	}

}
