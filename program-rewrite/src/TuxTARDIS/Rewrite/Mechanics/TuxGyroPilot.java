package TuxTARDIS.Rewrite.Mechanics;

import TuxTARDIS.Rewrite.Const;
import TuxTARDIS.Rewrite.Utils.Common;
import lejos.hardware.sensor.EV3GyroSensor;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;

/**
 * Our robot movement implementation
 */
public class TuxGyroPilot implements TuxPilot {
	/**
	 * Poměr otočení robota a kol
	 */
	private final double turn_rate;
	/**
	 * Obvod kol
	 */
	private final double forward_rate;
	/**
	 * Aktuální úhlová rychlost
	 */
	private int travelSpeed;
	/**
	 * Aktuální úhlová rychlost
	 */
	private int rotateSpeed;
	/**
	 * Pravý motor
	 */
	private final RegulatedMotor right;
	/**
	 * Levý motor
	 */
	private final RegulatedMotor left;
	/**
	 * Gyroskop
	 */
	private final EV3GyroSensor gyro;
	/**
	 * Tlačítko
	 */
	private final EV3TouchSensor touchFront;
	/**
	 * Tlačítko
	 */
	private final EV3TouchSensor touchRear;
	private final double travelback;
	/**
	 * Výchozí konstruktor
	 */
	public TuxGyroPilot(PilotProperties props) {
		turn_rate = (props.getTrackWidth() / props.getWheelDiameter()); // výpočty konstant
		forward_rate = props.getWheelDiameter() * Math.PI;
		this.travelback = props.getTravelback();
		this.right = props.getRightMotor(); // inicializace motorů
		this.left = props.getLeftMotor();
		this.touchFront = props.getFrontTouch();
		this.touchRear = props.getBackTouch();

		left.synchronizeWith(new RegulatedMotor[]{right}); // synchronizace

		this.gyro = props.getGyro();
		gyro.reset(); // reset & kalibrace gyroskopu
	}

	/**
	 * Spočítá otočení kol z otočení robota
	 *
	 * @param degree Otočení robota ve stupních
	 * @return Otočení kol (strany +- nebo -+)
	 */
	private float turn_size(double degree) {
		return (float)(turn_rate * degree);
	}

	/**
	 * Spočítá otočení kol ze vzdálenosti k ujetí
	 *
	 * @param distance Vzdálenost k ujetí
	 * @return Otočení kol (strany ++ nebo --)
	 */
	private float forward_size(double distance) {
		return (float)(distance / forward_rate * 360);
	}

	/**
	 * Ujede danou vzdálenost
	 *
	 * @param distance Vzdálenost k ujetí
	 */
	public void travel(double distance) {
		putSpeed(travelSpeed);
		float kola = forward_size(distance); // spočte otočení
		left.startSynchronization(); // otáčení
		left.rotate(Math.round(kola));
		right.rotate(Math.round(kola));
		left.endSynchronization();
		SampleProvider touch = distance >= 0 ? touchFront.getTouchMode() : touchRear.getTouchMode();
		while(Common.getFirst(touch)==0 &&  left.isMoving() && right.isMoving())
			Thread.yield();
		stop();
	}

	/**
	 * Čeká, dokud se motory nezastaví
	 */
	private void waitComplete() {
		while (left.isMoving() || right.isMoving()) {
			left.waitComplete();
			right.waitComplete();
		}
	}

	/**
	 * Nastaví rychlost motorů
	 *
	 * @param speed Úhlová rychlost v °/s
	 */
	private void putSpeed(int speed) {
		left.startSynchronization();
		right.setSpeed(speed);
		left.setSpeed(speed);
		left.endSynchronization();
	}

	/**
	 * Zapne robota dopředu
	 */
	public void forward() {
		putSpeed(travelSpeed);
		left.startSynchronization();
		left.forward();
		right.forward();
		left.endSynchronization();
	}

	@Override
	public void backward() {
		putSpeed(travelSpeed);
		left.startSynchronization();
		left.backward();
		right.backward();
		left.endSynchronization();
	}

	/**
	 * Zastaví robota
	 */
	public void stop() {
		left.startSynchronization();
		left.stop(true);
		right.stop(true);
		left.endSynchronization();
	}


	/**
	 * Zastaví robota
	 */
	public void setAcceleration(int accel) {
		left.startSynchronization();
		left.setAcceleration(accel);
		right.setAcceleration(accel);
		left.endSynchronization();
	}
	@Override
	public void setTravelSpeed(double v) {
		this.travelSpeed = (int) (Math.min(left.getMaxSpeed(), right.getMaxSpeed()) * v / 100); // přepočte procenta na °/s
	}


	/**
	 * Zarovná robota
	 */
	public void alignFront() {
		putSpeed(travelSpeed);
		SampleProvider button = touchFront.getTouchMode(); // vzorkovač tlačítka
		forward(); // jeď dopředu
		while (Common.getFirst(button) != 1)
			Thread.yield(); // už nemáme nic k práci
		Delay.msDelay(Const.TRAVELBACK_DELAY); // čekej
		travel(-travelback); // popojeď zpět + zastav
	}
	/**
	 * Zarovná robota
	 */
	public void alignBack() {
		putSpeed(travelSpeed);
		SampleProvider button = touchRear.getTouchMode(); // vzorkovač tlačítka
		backward(); // jeď dopředu
		while (Common.getFirst(button) != 1)
			Thread.yield(); // už nemáme nic k práci
		Delay.msDelay(Const.TRAVELBACK_DELAY); // čekej
		travel(travelback); // popojeď zpět + zastav
	}

	@Override
	public void rotate(double v) {
		putSpeed(rotateSpeed);
		SampleProvider data = gyro.getAngleMode(); // získání vzorkovače
		Common.getFirst(data); // HACK pro rozběhnutí vzorkovače
		double expectedValue = Common.getFirst(data)+v;

		int kola = Math.round(turn_size(v)); // získání potřebného otočení kol

		left.startSynchronization(); // synchronizované točení
		left.rotate(-kola, true);
		right.rotate(kola, true);
		left.endSynchronization();
		waitComplete(); // počká na konec

		while (true) { // "donekonečna"
			float angle = Common.getFirst(data); // získej úhel otočení
			double odchylka = expectedValue - angle; // spočti odchylku
			if (Math.abs(odchylka) < Const.MAXERROR) // pokud je odchylka malá
				break; // přeruš nekonečno

			int fix = Math.round(turn_size((int) (Const.TURN_KP * odchylka))); // spočti opravu
			left.startSynchronization(); // a dotoč
			left.rotate(-fix, true);
			right.rotate(fix, true);
			left.endSynchronization();
			waitComplete();
		}
	}

	@Override
	public void setRotateSpeed(double v) {
		this.rotateSpeed = (int) (Math.min(left.getMaxSpeed(), right.getMaxSpeed()) * v / 100); // přepočte procenta na °/s
	}
}