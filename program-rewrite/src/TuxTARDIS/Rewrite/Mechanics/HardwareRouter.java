package TuxTARDIS.Rewrite.Mechanics;

import TuxTARDIS.Rewrite.Utils.Common;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.SampleProvider;

/**
 * Hardware physical interface
 */
public class HardwareRouter implements TuxPilot, MeasureProvider {
	private TuxPilot pilot;
	private EV3UltrasonicSensor sonic;
	private SampleProvider sonicSample;
	private PilotProperties other;
	protected HardwareRouter(){
	}
	public HardwareRouter(PilotType pilotType,TuxPilot.PilotProperties properties,
	                      EV3UltrasonicSensor sonic){
		if(pilotType==PilotType.leJOS)
			pilot = new TuxCompassPilotProxy(properties);
		if(pilotType==PilotType.TuxovaTARDIS)
			pilot = new TuxGyroPilot(properties);
		this.sonic = sonic;
		this.sonicSample = this.sonic.getDistanceMode();
		this.other = properties;
	}

	@Override
	public void travel(double distance) {
		pilot.travel(distance);
	}

	@Override
	public void stop() {
		pilot.stop();
	}

	@Override
	public void rotate(double angle) {
		pilot.rotate(angle);
	}

	@Override
	public void alignBack() {
		pilot.alignBack();
	}

	@Override
	public void alignFront() {
		pilot.alignFront();
	}

	@Override
	public void forward() {
		pilot.forward();
	}

	@Override
	public void backward() {
		pilot.backward();
	}

	@Override
	public void setTravelSpeed(double speed) {
		pilot.setTravelSpeed(speed);
	}

	@Override
	public void setRotateSpeed(double speed) {
		pilot.setRotateSpeed(speed);
	}

	@Override
	public void setAcceleration(int accel) {
		pilot.setAcceleration(accel);
	}

	@Override
	public float getSonic() {
		return Common.getFirst(sonicSample);
	}

	@Override
	public float getSonic(int rotateQ, boolean goBack) {
		pilot.rotate(rotateQ*90);
		float value = getSonic();
		if(goBack)
			pilot.rotate(-rotateQ*90);
		return value;
	}

	@Override
	public float getGyro() {
		return Common.getFirst(other.getGyro().getAngleMode());
	}

	@Override
	public boolean getFrontTouch() {
		return Common.getFirst(other.getBackTouch().getTouchMode())==1;
	}

	@Override
	public boolean getBackTouch() {
		return Common.getFirst(other.getFrontTouch().getTouchMode())==1;
	}
	@Override
	public void resetGyro() {
		other.getGyro().reset();
	}
}
