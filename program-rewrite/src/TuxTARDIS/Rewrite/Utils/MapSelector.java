package TuxTARDIS.Rewrite.Utils;

import TuxTARDIS.Rewrite.Const;
import lejos.hardware.Button;
import lejos.hardware.Sound;
import lejos.hardware.lcd.LCD;
import lejos.utility.Delay;


/**
 * Menu mapy
 */
public class MapSelector {
	/**
	 * Výzva k zadání
	 */
	private final String prompt;

	/**
	 * Konstruktor
	 *
	 * @param prompt Výzva
	 */
	public MapSelector(String prompt) {
		this.prompt = prompt;
	}

	/**
	 * Zeptá se na číslo, počká na zadání a vrátí jej
	 *
	 * @param min Minimální hodnota (včetně)
	 * @param max Maximální hodnota (včetně)
	 * @param def Výchozí hodnota
	 * @return Zadané číslo
	 */
	public int getNumber(int min, int max, int def) {
		int value = def; // nastaví hodnotu na výchozí
		LCD.clear(); // vyčistí displej
		LCD.drawString(prompt, 0, 0); // vypíše výzvu
		update(value); // zapíše na displej
		Button.setKeyClickVolume(0); // vypne pípání tlačítek

		while (true) { // "donekonečna"
			if (Button.ENTER.isDown()) { // pokud je zmáčknut enter
				Sound.playTone(500, 50); // pípni
				break; // ukonči smyčku
			}
			if (Button.RIGHT.isDown() && Button.LEFT.isUp()) { // pokud je pravé zmáčknuto a levé ne
				Sound.playTone(500, 50); // pípni
				if (value < max) // uprav hodnotu a vypiš
					value++;
				else if (value == max)
					value = min;
				update(value);
			}
			if (Button.LEFT.isDown() && Button.RIGHT.isUp()) { // pokud je pravé zmáčknuto a levé ne
				Sound.playTone(500, 50); // pípni
				if (value > min) // uprav hodnotu a vypiš
					value--;
				else if (value == min)
					value = max;
				update(value);
			}
			Delay.msDelay(Const.MAPSELECT_DELAY); // čekej
		}
		LCD.clear(); // vyčistí po sobě displej
		return value;
	}

	/**
	 * Vypíše hodnotu na displej
	 *
	 * @param value Hodnota k vypsání
	 */
	private void update(int value) {
		LCD.drawString(String.format("%2d", value), 0, 1);
	}
}
