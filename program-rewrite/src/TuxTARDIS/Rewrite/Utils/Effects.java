package TuxTARDIS.Rewrite.Utils;

import lejos.hardware.Sound;

/**
 * Efekty
 */
public class Effects {
	/**
	 * Zapípá
	 */
	public static void play_tune() {
		Sound.setVolume(100);
		Sound.twoBeeps();
		Sound.beepSequenceUp();
		Sound.beepSequenceUp();
	}
	public static void twobeep(){
		new Thread() {
			public void run() {
				Sound.twoBeeps();
			}
		}.start(); // asynchronní pípnutí
	}
}
