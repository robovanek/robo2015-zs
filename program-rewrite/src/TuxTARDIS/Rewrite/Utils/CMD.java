package TuxTARDIS.Rewrite.Utils;
/**
 * Výčet příkazů
 */
public enum CMD {
	Forward, Backward, ToLeft, ToRight
}