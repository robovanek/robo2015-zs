package TuxTARDIS.Rewrite.Utils;

/**
 * Integer range
 */
public class FloatRange {
	private float minimum=0;
	private float maximum=0;
	private float correct=0;
	/**
	 * Creates integer range
	 */
	public FloatRange(float correct, int limit){
		this.minimum = correct-limit;
		this.maximum = correct+limit;
		this.correct = correct;
	}
	/**
	 * Check if this  number is in range
	 * @param value Number
	 * @return true if in range
	 */
	public boolean fits(double value) {
		return minimum <= value && value <= maximum;
	}
	public void set(int min, int max){
		this.minimum = min;
		this.maximum = max;
	}
	public void set(FloatRange range){
		this.minimum = range.minimum;
		this.maximum = range.maximum;
	}

	public float getCorrect() {
		return correct;
	}
}
