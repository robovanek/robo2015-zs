package TuxTARDIS.Rewrite.Utils;

import lejos.robotics.SampleProvider;

import java.util.Random;
import java.util.concurrent.BlockingQueue;

/**
 * Obecné funkce, které se nehodí do specifické třídy.
 */
public final class Common {
	private static final Random rand = new Random();
	/**
	 * Pomocná funkce pro hledání čísla v poli
	 *
	 * @param o Číslo k nalezení
	 * @param a Pole k prohledání
	 * @return Jestli je dané číslo v poli přítomen
	 */
	public static boolean aContainsO(Integer o, Integer[] a) {
		for (Integer element : a) // projdi všechny prvky pole
			if (element.equals(o)) // pokud se čísla shodují
				return true; // číslo nalezeno
		return false; // číslo nenalezeno
	}

	/**
	 * Získá první hodnotu získanou ze vzorkovače
	 *
	 * @param sampler Vzorkovač
	 * @return První vzorek z pole
	 */
	public static float getFirst(SampleProvider sampler) {
		float[] blbost = new float[sampler.sampleSize()];
		sampler.fetchSample(blbost, 0);
		return blbost[0];
	}

	private Common() {
	} // zbytečný (ale nutný) konstruktor
	public static int randInt(int min, int max) {
		return rand.nextInt((max - min) + 1) + min;
	}
	public static <T> T getNext(BlockingQueue<T> input){
		try{ return input.take();}
		catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
}
