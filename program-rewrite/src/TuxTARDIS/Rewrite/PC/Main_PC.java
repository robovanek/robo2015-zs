package TuxTARDIS.Rewrite.PC;

import TuxTARDIS.Rewrite.Const;
import TuxTARDIS.Rewrite.Navigation.Map;
import TuxTARDIS.Rewrite.Navigation.Path;
import TuxTARDIS.Rewrite.Navigation.TileType;
import TuxTARDIS.Rewrite.VM.CPU;
import TuxTARDIS.Rewrite.VM.Instruction;
import lejos.hardware.Sound;

import java.io.*;
import java.util.concurrent.BlockingQueue;

/**
 * Main class for computer
 */
class Main_PC {
	public static void main(String[] args){
		Const.CONFIG = "/home/kuba/robo2015-zs/program-rewrite/pc.conf";
		Const.load();

		int mn = getInt("Enter map number: "); // zeptá se na číslo mapy
		String mapStr = load_txt(Const.MAP_DIR + "/" + mn + ".map"); // načte mapu
		Map map = new Map(mapStr); // inicializuje mapu
		Path path = new Path(mapStr);
		BlockingQueue<Instruction> exe = CPU.streamize(map,path);

		FakeBackend hw = new FakeBackend(map);
		CPU cpu = new CPU(hw);

		System.out.println("* ENTER -> RUN *");
		hw.waitForPress();

		cpu.runStream(exe, map);
		while (hw.getContinue()){
			Map newMap = new Map(mapStr);
			newMap.curPos = map.curPos;
			newMap.curOrient = map.curOrient;
			newMap.set(map.curPos, TileType.LightOff);
			path = Path.genPath_BFS(new Map(newMap));
			cpu.runStream(CPU.streamize(newMap, path), newMap);
		}

	}
	/**
	 * Přečte texťák
	 *
	 * @param name Jméno souboru
	 * @return Text souboru
	 */
	public static String load_txt(String name) {
		StringBuilder sb = new StringBuilder(512); // buffer souboru
		try { // proti vyjímce
			FileInputStream stream = new FileInputStream(name); // otevře soubor
			Reader r = new InputStreamReader(stream, "UTF-8"); // otevře čteč souboru
			int c; // přečte soubor
			while ((c = r.read()) != -1) {
				sb.append((char) c);
			}
			stream.close(); // zavře soubor
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return sb.toString(); // vrátí buffer
	}
	public static int getInt(String name){
		try {
			System.out.print(name);
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			return Integer.parseInt(in.readLine());
		}catch(Exception e){
			e.printStackTrace();
			throw new RuntimeException("SHIT INFO: "+e.getLocalizedMessage());
		}
	}
}
