package TuxTARDIS;

import lejos.hardware.port.MotorPort;
import lejos.hardware.port.Port;
import lejos.hardware.port.SensorPort;
import java.util.HashMap;

/**
 * Exported constants
 */
public class Const {
	// MAIN
	/**
	 * Port tlačítka
	 */
	public static final Port TOUCH_PORT = SensorPort.S1;
	/**
	 * Celkový počet map
	 */
	public static int MAPS;
	/**
	 * Rychlost robota v procentech z maximální rychlosti
	 */
	public static int SPEED;
	/**
	 * Složka, ve které jsou mapy, bez koncového lomítka
	 */
	public static String MAP_DIR;

	// MECHANICS
	/**
	 * Průměr kol
	 */
	public static double DIAMETER;
	/**
	 * Vzdálenost mezi koly (pozor, adjusted)
	 */
	public static double TRACKWIDTH;
	/**
	 * Maximální tolerovaná odchylka při otáčení
	 */
	public static int MAXERROR;
	/**
	 * Multiplikátor oprav
	 */
	public static float TURN_KP;
	/**
	 * Zrychlení motorů ve °/(s*s)
	 */
	public static int ACCEL;
	/**
	 * Stall lag
	 */
	public static int STALL_LAG;
	/**
	 * Délka stallu
	 */
	public static int STALL_TIME;
	/**
	 * Port levého motoru
	 */
	public static final Port LEFT_PORT = MotorPort.B;
	/**
	 * Port pravého motoru
	 */
	public static final Port RIGHT_PORT = MotorPort.C;
	/**
	 * Port gyroskopu
	 */
	public static final Port GYRO_PORT = SensorPort.S2;

	// SCHEDULER
	/**
	 * Délka pole v centimetrech
	 */
	public static int TILE_LENGTH;
	/**
	 * Jestli povolit kumulování příkazů v cache
	 */
	public static boolean ENABLE_CACHING;
	/**
	 * Jestli povolit zarovnávání robota při překročení virtuálního limitu nepřesností
	 */
	public static boolean ENABLE_ALIGN_COUNTER;
	/**
	 * Virtuální limit nepřesností
	 */
	public static int ERROR_LIMIT; // WARN neladěno
	/**
	 * Popojetí zpět při zarovnávání
	 */
	public static int TRAVELBACK;
	/**
	 * Popojetí zpět při zarovnávání (milisekundy)
	 */
	public static int TRAVELBACK_DELAY;
	/**
	 * Jestli povolit zarovnání robota, když je přímo před ním zeď
	 */
	public static boolean ENABLE_FWD_ALIGN;
	/**
	 * Jestli provést pouze n-té zarovnání o zeď před robotem, -1 pro zákaz přeskakování
	 */
	public static int FWD_ALIGN_SKIP; // -1 pro zákaz skipu

	// MAPSELECTOR
	/**
	 * Prodleva mezi zjišťováním stisknutí tlačítek (milisekundy)
	 */
	public static int MAPSELECT_DELAY;

	// DYNAMICKÝ CONST
	/**
	 * Konfigurační soubor
	 */
	private static final String CONFIG = "/home/lejos/programs/tux.conf";

	/**
	 * Regulační konstanta
	 */
	public static float FWD_KP;
	/**
	 * Jestli povolit gyro-korekci při zarovnávání
	 */
	public static boolean ENABLE_FORWARD_GYRO;
	/**
	 * Jestli povolit gyro-korekci při jízdě bez zarovnávání, polofunkční
	 */
	public static boolean ENABLE_TRAVEL_GYRO;
	/**
	 * Načte hodnoty ze souboru
	 */
	public static void load(){
		String data = Main.load_txt(CONFIG); // načte konfigurák
		data = data.replaceAll("\\s", ""); // odstranění mezer
		data = data.replaceAll("\\n", ""); // odstranění nových řádků
		String[] settings = data.split(";"); // rozdělí na jednotlivá nastavení
		HashMap<String,String> map = new HashMap<>(settings.length); // slovník key -> value
		for(String keypair:settings){ // projde nastavení
			String[] separated = keypair.split("="); // rozdělí na key a value
			map.put(separated[0],separated[1]); // zapíše do slovníku
		}
		apply(map); // předá nastavení ze slovníku do hodnot této třídy
	}

	/**
	 * Načte hodnoty konstant ze slovníku
	 * @param settings Slovník nastavení
	 */
	private static void apply(HashMap<String,String> settings){
		// String
		MAP_DIR = settings.get("MAP_DIR");
		// Integer
		MAPS = Integer.parseInt(settings.get("MAPS"));
		SPEED = Integer.parseInt(settings.get("SPEED"));
		ACCEL = Integer.parseInt(settings.get("ACCEL"));
		MAXERROR = Integer.parseInt(settings.get("MAXERROR"));
		TILE_LENGTH = Integer.parseInt(settings.get("TILE_LENGTH"));
		ERROR_LIMIT = Integer.parseInt(settings.get("ERROR_LIMIT"));
		TRAVELBACK = Integer.parseInt(settings.get("TRAVELBACK"));
		TRAVELBACK_DELAY = Integer.parseInt(settings.get("TRAVELBACK_DELAY"));
		FWD_ALIGN_SKIP = Integer.parseInt(settings.get("FWD_ALIGN_SKIP"));
		MAPSELECT_DELAY = Integer.parseInt(settings.get("MAPSELECT_DELAY"));
		STALL_LAG = Integer.parseInt(settings.get("STALL_LAG"));
		STALL_TIME = Integer.parseInt(settings.get("STALL_TIME"));
		// Boolean
		ENABLE_CACHING = Boolean.parseBoolean(settings.get("ENABLE_CACHING"));
		ENABLE_ALIGN_COUNTER = Boolean.parseBoolean(settings.get("ENABLE_ALIGN_COUNTER"));
		ENABLE_FWD_ALIGN = Boolean.parseBoolean(settings.get("ENABLE_FWD_ALIGN"));
		ENABLE_FORWARD_GYRO = Boolean.parseBoolean(settings.get("ENABLE_FORWARD_GYRO"));
		ENABLE_TRAVEL_GYRO = Boolean.parseBoolean(settings.get("ENABLE_TRAVEL_GYRO"));

		// Double
		DIAMETER = Double.parseDouble(settings.get("DIAMETER"));
		TRACKWIDTH = Double.parseDouble(settings.get("TRACKWIDTH"));
		// Float
		TURN_KP = Float.parseFloat(settings.get("TURN_KP"));
		FWD_KP = Float.parseFloat(settings.get("FWD_KP"));
	}
}
