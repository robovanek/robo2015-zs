package TuxTARDIS;

/**
 * Ladící komponenta
 */
public class Debug {
	/**
	 * Vypíše informační hlášku
	 *
	 * @param where Odkud informace přichází (zkratka)
	 * @param data  Samotná informace
	 */
	public static void info(String where, String data) {
		System.out.println("[" + where + "] " + data);
	}
}
