package TuxTARDIS.Navigation;

import TuxTARDIS.Utils.Common;
import java.util.*;

/**
 * Trasa/Cesta (Queue)
 */
public class Path {
	private static final int WALL = -1;
	/**
	 * Interní reprezentace trasy, fronta souřadnic
	 */
	public Queue<Pos> path;

	/**
	 * Konstruktor
	 *
	 * @param map Obsah cesty
	 */
	public Path(String map) {
		this.load(map);
	}

	/**
	 * Načte cestu z textového obsahu
	 *
	 * @param data Textový obsah cesty
	 */
	public void load(String data) {
		// PARSER
		Integer[][][] map = new Integer[Map.sizeX][Map.sizeY][]; // mapa seznamů pořadí
		data = data.replaceAll("\\s", ""); // odstraní mezery, nové řádky
		data = data.replaceAll("\\n", "");
		String fields[] = data.split(","); // rozdělí text na jednotlivá pole
		int index = 0; // iterace
		for (int y = 0; y < Map.sizeY; y++) { // procházej po sloupcích
			for (int x = 0; x < Map.sizeX; x++) { // v každém sloupci procházej po řádcích
				if (!fields[index].equals("X")) { // pokud zde není zeď
					List<Integer> elements = new ArrayList<>(); // seznam pořadí
					for (String element : fields[index].split("\\+")) // rozdělí pole na jednotlivá pořadí
						elements.add(Integer.parseInt(element)); // a ta přidá do seznam

					map[x][y] = elements.toArray(new Integer[elements.size()]); // převede seznam na pole a uloží
				} else // když zde je zeď
					map[x][y] = new Integer[]{WALL}; // nastav zde zeď
				index++; // posuň se v textu
			}
		}

		// BFS - hledání cesty
		Queue<Pos> path = new LinkedList<>(); // samotná cesta
		Pos frontier = Map.center; // aktuální pozice, na začátku jsme na startu
		index = 0; // aktuální pořadí

		while (frontier != Pos.none) // dokud máme co procházet
		{
			Pos next = Pos.none; // budoucí průchod
			Pos[] adjacent = Pos.adjacent(frontier); // získáme okolí pozice
			for (Pos v : adjacent) // projdeme okolí pozice
			{
				if (Common.aContainsO(index + 1, map[v.x][v.y])) { // pokud pozice obsahuje správné pořadí
					path.add(v); // přidej do cesty
					next = v; // nastav jako aktuální pozici
					break; // vyskoč
				}
			}
			index++; // posuň se v cestě
			frontier = next; // přeřazení, minulá budoucnoust = současnost
		}
		this.path = path; // nastav cestu
	}
}
