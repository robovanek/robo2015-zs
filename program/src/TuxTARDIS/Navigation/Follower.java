package TuxTARDIS.Navigation;
import TuxTARDIS.Scheduler;

/**
 * Procházeč cesty
 */
public class Follower {
	/**
	 * Mapa
	 */
	private final Map map;
	/**
	 * Cache
	 */
	private final Scheduler scheduler;

	/**
	 * Výchozí konstruktor
	 *
	 * @param map   Mapa bludiště
	 * @param sched Cache pro příkazy
	 */
	public Follower(Map map, Scheduler sched) {
		this.map = map;
		this.scheduler = sched;
	}

	/**
	 * Projde celou cestu a nacpe ji do cache, která ji postupně vykoná.
	 *
	 * @param p Cesta k projití
	 */
	public void Follow(Path p) {
		while (p.path.size() > 0) { // dokud je něco dál
			Pos pozice = p.path.poll(); // vezmi souřadnice další pozice
			if (pozice == map.curPos) // pokud už na této pozici jsme
				continue; // přeskoč
			int direction = Pos.directionOf(map.curPos, pozice); // zjisti směr jízdy
			int difference = MapOrient.difference(map.curOrient, direction); // zjisti odklonění od současného směru
			if (difference == 0) // pokud máme jet rovně
				scheduler.forward(); // jeď rovně

			else if (difference == 2) // pokud máme jet dozadu
				scheduler.backward(); // jeď dozadu

			else if (difference == -1) { // pokud máme jet doleva
				scheduler.left(); // zahni doleva
				scheduler.forward(); // jeď rovně
			} else if (difference == 1) { // pokud máme jet doprava
				scheduler.right(); // zahni doprava
				scheduler.forward(); // jeď rovně
			}
		}
		scheduler.dispose(); // dodělej cache, v Main.main() je projistotu znovu
	}
}