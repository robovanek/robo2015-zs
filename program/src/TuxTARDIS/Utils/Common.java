package TuxTARDIS.Utils;

import lejos.robotics.SampleProvider;

/**
 * Obecné funkce, které se nehodí do specifické třídy.
 */
public final class Common {
	/**
	 * Pomocná funkce pro hledání čísla v poli
	 *
	 * @param o Číslo k nalezení
	 * @param a Pole k prohledání
	 * @return Jestli je dané číslo v poli přítomen
	 */
	public static boolean aContainsO(Integer o, Integer[] a) {
		for (Integer element : a) // projdi všechny prvky pole
			if (element.equals(o)) // pokud se čísla shodují
				return true; // číslo nalezeno
		return false; // číslo nenalezeno
	}

	/**
	 * Získá první hodnotu získanou ze vzorkovače
	 *
	 * @param sampler Vzorkovač
	 * @return První vzorek z pole
	 */
	public static float getFirst(SampleProvider sampler) {
		float[] blbost = new float[sampler.sampleSize()];
		sampler.fetchSample(blbost, 0);
		return blbost[0];
	}

	private Common() {
	} // zbytečný (ale nutný) konstruktor
}
