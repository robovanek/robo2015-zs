cmake_minimum_required(VERSION 3.5)
project(program_cpp)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "out")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(CURSES_NEED_NCURSES TRUE)
find_package(Curses REQUIRED)
add_definitions(-DHAS_CURSES)

include_directories(${CMAKE_SOURCE_DIR}/external/include ${CURSES_INCLUDE_DIRS})
find_package(Threads)

set(SOURCE_FILES src/main.cpp src/point.cpp src/point.h src/tuxtardis.h src/map.cpp src/map.h src/path.h src/parser.cpp src/parser.h src/effect.h src/config.h src/processor.cpp src/processor.h)
add_executable(program_cpp ${SOURCE_FILES})

target_link_libraries(program_cpp PRIVATE ${CMAKE_SOURCE_DIR}/external/lib/libev3dev.a)
target_link_libraries(program_cpp PRIVATE ${CURSES_LIBRARIES})
target_link_libraries(program_cpp PRIVATE ${CMAKE_SOURCE_DIR}/external/lib/librobofat.a)
target_link_libraries(program_cpp PRIVATE ${CMAKE_THREAD_LIBS_INIT})

SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O3 -flto")
SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -O3 -flto")
