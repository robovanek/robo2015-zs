/*
 * RoboFAT, a small C++ robotic library for ev3dev
 * Copyright (C) 2016  Jakub Vaněk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Screen I/O.
 */

#ifndef ROBOFAT_DISPLAY_H
#define ROBOFAT_DISPLAY_H

#include <string>
#include <vector>

#ifdef HAS_CURSES

#include <ncurses.h>

#endif
namespace robofat {
	/**
	 * \brief Screen I/O
	 *
	 * @author Jakub Vaněk <vanek.jakub4@seznam.cz>
	 */
	class display {
	public:
#ifdef HAS_CURSES
		/**
		 * \brief EV3 keys
		 */
		enum keys {
			/**
			 * \brief Unknown key
			 */
					key_unknown = 0,
			/**
			 * \brief The up arrow button.
			 */
					key_up = KEY_UP,
			/**
			 * \brief The down arrow button.
			 */
					key_down = KEY_DOWN,
			/**
			 * \brief The left arrow button.
			 */
					key_left = KEY_LEFT,
			/**
			 * \brief The right arrow button.
			 */
					key_right = KEY_RIGHT,
			/**
			 * \brief The confirm (center) button.
			 */
					key_enter = KEY_ENTER,
			/**
			 * \brief The leave (out) button.
			 */
					key_escape = KEY_BACKSPACE
		};
		/**
		 * \brief Font attributes
		 */
		enum attrib {
			/**
			 * \brief No attributes
			 */
					attr_none = 0,
			/**
			 * \brief Normal display (no highlight)
			 */
					attr_normal = A_NORMAL,
			/**
			 * \brief Best highlighting mode of the terminal.
			 */
					attr_standout = A_STANDOUT,
			/**
			 * \brief Underlining
			 */
					attr_underline = A_UNDERLINE,
			/**
			 * \brief Reverse video
			 */
					attr_reverse = A_REVERSE,
			/**
			 * \brief Blinking
			 */
					attr_blink = A_BLINK,
			/**
			 * \brief Half bright
			 */
					attr_dim = A_DIM,
			/**
			 * \brief Extra bright or bold
			 */
					attr_bold = A_BOLD
		};
#endif

		/**
		 * \brief Initializes ncurses.
		 */
		display();

		/**
		 * \brief Deinitializes ncurses.
		 */
		~display();

#ifdef HAS_CURSES

		/**
		 * \brief Read an EV3 button from main window.
		 *
		 * @return      The pressed button or `key_unknown`.
		 */
		keys getkey();

#endif
#ifdef HAS_CURSES

		/**
		 * \brief Print a string into the main window.
		 *
		 * @param str           String to print.
		 * @param attributes    Mask of attributes of the string.
		 */
		void print(const std::string &str, attrib attributes = attr_none);

#else
		/**
		 * \brief Print a string into the main window.
		 *
		 * @param str           String to print.
		 */
		void print(const std::string &str);
#endif
#ifdef HAS_CURSES

		/**
		 * \brief Print a string into the center of the main window.
		 *
		 * @param str           String to print.
		 * @param attributes    Mask of attributes of the string.
		 */
		void print_centered(const std::string &str, attrib attributes = attr_none);

#endif

		/**
		 * \brief Print beginning of init step.
		 *
		 * @param part      Part of the program signalizing the step.
		 */
		void init_begin(const std::string &part);

		/**
		 * \brief Append message to current init step.
		 *
		 * Note: this must be called after am {code init_begin(part);} call.
		 *
		 * @param message   Message to append.
		 */
		void init_msg(const std::string &message);

		/**
		 * \brief Print end of init step.
		 *
		 * Note: this must be called after am {code init_begin(part);} call.
		 *
		 * @param success   Whether the init action was successful.
		 */
		void init_end(bool success);

		/**
		 * \brief Print init log information.
		 *
		 * @param msg       Informational message.
		 */
		void init_log(const std::string &msg);

#ifdef HAS_CURSES

		/**
		 * \brief Start an interactive menu and return what the user has selected.
		 *
		 * @param prompt    Selection prompt.
		 * @param options   Available options.
		 * @param select    Default option index.
		 * @param can_exit  Whether the user can cancel the selection with EXIT button.
		 * @return          Index of selected option or -1 if user cancelled the menu.
		 */
		int show_menu(const std::string &prompt,
		              const std::vector<std::string> &options,
		              int select = 0, bool can_exit = true);

		/**
		 * \brief Clear the screen and move the cursor to the beginning.
		 */
		void clear_display();

#endif
	private:
#ifdef HAS_CURSES

		/**
		 * \brief Read an EV3 button from the specified window.
		 *
		 * @param wnd   Window to read from.
		 * @return      The pressed button or `key_unknown`.
		 */
		keys getkey(WINDOW *wnd);

		/**
		 * \brief Draw a menu
		 *
		 * @param wnd           Window for drawing the menu.
		 * @param prompt        Selection prompt.
		 * @param options       Available options.
		 * @param select        Index of selected option.
		 * @param first_list_y  Relative Y-coordinate of first list element.
		 * @param first_x       Relative X-coordinate of the left side.
		 */
		void menu_draw(WINDOW *wnd,
		               const std::string &prompt,
		               const std::vector<std::string> &options,
		               const int select,
		               const int first_list_y,
		               const int first_x);

		/**
		 * \brief Get the length of the longest string.
		 *
		 * @param prompt    Selection prompt.
		 * @param options   Available options.
		 */
		int get_longest(const std::string &prompt,
		                const std::vector<std::string> &options);

		/**
		 * \brief Prepare for writing on new line.
		 *
		 * @param y     Current Y-coordinate. This function puts here Y-coordinate for move or mvprintw.
		 */
		void newline(int &y);

		int width = 0;  ///< Width of the terminal.
		int height = 0; ///< Height of the terminal.
		int msg_y = 0;    ///< Original init begin Y-coordinate from overwriting status.
#else
		std::string last_part;
#endif
	};

#ifdef HAS_CURSES

	/**
	 * \brief Operator for adding attributes
	 *
	 * @param a     First attribute
	 * @param b     Second attribute
	 * @return      Mixed attribute
	 */
	inline display::attrib operator|(const display::attrib a, const display::attrib b) {
		return static_cast<display::attrib>(static_cast<int>(a) | static_cast<int>(b));
	}

#endif
}

#endif //ROBOFAT_DISPLAY_H
