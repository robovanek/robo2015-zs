/*
 * RoboFAT, a small C++ robotic library for ev3dev
 * Copyright (C) 2016  Jakub Vaněk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Motor and sensor initialization functions.
 */

#ifndef ROBOFAT_PORTS_H
#define ROBOFAT_PORTS_H

#include <ev3dev.h>
#include <ostream>
#include <sstream>
#include "display.h"

using namespace ev3dev;
using std::ostream;

namespace robofat {
	/**
	 * \brief Allocate a device and check if it's connected.
	 *
	 * @param port  Port where the device should be connected.
	 * @return Pointer to heap if the device is connected, NULL otherwise.
	 */
	template<typename T>
	T *get_device_heap(const address_type &port) {
		T *device = new T(port);
		if (!device->connected()) {
			delete device;
			return nullptr;
		}
		return device;
	}

#ifdef HAS_CURSES

	/**
	 * \brief Allocate a device and ask user if it's not connected.
	 *
	 * @param port      Port where the device should be connected.
	 * @param display   Display for question.
	 * @param motor     True if the device is a motor, false otherwise.
	 * @param what      Info for the user.
	 * @return Pointer to heap.
	 */
	template<typename T>
	extern T *get_device_heap_prompt(address_type port,
	                                 robofat::display &display,
	                                 bool motor,
	                                 const std::string &what) {
		// choice menus
		static std::vector<std::string> motor_menu = {"Retry", "Port A", "Port B", "Port C", "Port D"};
		static std::vector<std::string> sensor_menu = {"Retry", "Port 1", "Port 2", "Port 3", "Port 4"};
		// while we don't have the device
		while (true) {
			// get device and check
			T *device = get_device_heap<T>(port);
			if (device != nullptr)
				return device;
			// check failed, ask the user
			std::stringstream stream;
			stream << "Port " << port.substr(motor ? 3 : 2) << ":";
			stream << what;
			stream << '?';
			int choice = display.show_menu(stream.str(),
			                               motor ? motor_menu : sensor_menu,
			                               0, false);
			switch (choice) {
				case 1:
					port = motor ? OUTPUT_A : INPUT_1;
					break;
				case 2:
					port = motor ? OUTPUT_B : INPUT_2;
					break;
				case 3:
					port = motor ? OUTPUT_C : INPUT_3;
					break;
				case 4:
					port = motor ? OUTPUT_D : INPUT_4;
					break;
				default:
					break;
			}
		}
	}

#endif

	/**
	 * \brief 'Device was not found' exception
	 */
	class device_not_found {
	public:
		device_not_found(const std::string &message,
		                 const device_type &type,
		                 const address_type &port);

		device_type get_device_type();

		address_type get_port();

		ostream &operator<<(ostream &stream);

	private:
		const device_type &type;
		const address_type &port;
		const std::string &message;
	};
};


#endif //ROBOFAT_PORTS_H
