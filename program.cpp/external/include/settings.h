/*
 * RoboFAT, a small C++ robotic library for ev3dev
 * Copyright (C) 2016  Jakub Vaněk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Settings storage.
 */

#ifndef ROBOFAT_SETTINGS_H
#define ROBOFAT_SETTINGS_H

#include <iostream>
#include <fstream>
#include <string>
#include <unordered_map>

#include "ev3dev.h"

namespace robofat {
	/**
	 * \brief Configuration storage
	 *
	 * @author Jakub Vaněk <vanek.jakub4@seznam.cz>
	 */
	class settings {
	public:
		/**
		 * \brief Construct new settings.
		 */
		settings();

		/**
		 * \brief Destruct new settings (free the backing store).
		 */
		~settings();

		/**
		 * \brief Load setttings from file to this instance.
		 *
		 * @param input         Input stream with data.
		 * @param lower_keys    Whether the keys should be transformed to lowercase.
		 * @param lower_values  Whether the values should be transformed to lowercase.
		 * @return        0 on success, 1 on failure.
		 */
		int load(std::istream &input, bool lower_keys, bool lower_values);

		/**
		 * \brief Get string from config.
		 *
		 * @param name   Name of the option.
		 * @return       String value of the option.
		 */
		std::string get(const std::string name) const;

		/**
		 * \brief Get integer from config.
		 *
		 * @param name   Name of the option.
		 * @return       Integer value of the option.
		 */
		int get_integer(const std::string name) const;

		/**
		 * \brief Get single-precision floating point number from config.
		 *
		 * @param name   Name of the option.
		 * @return       Float value of the option.
		 */
		float get_float(const std::string name) const;

		/**
		 * \brief Get double-precision floating point number from config.
		 *
		 * @param name   Name of the option.
		 * @return       Double value of the option.
		 */
		double get_double(const std::string name) const;

		/**
		 * \brief Get boolean from config.
		 *
		 * @param name   Name of the option.
		 * @return       Bool value of the option.
		 */
		bool get_bool(const std::string name) const;

		/**
		 * \brief Get port from config.
		 *
		 * @param name   Name of the option.
		 * @return       Port value of the option.
		 */
		ev3dev::address_type get_port(const std::string name) const;

		/**
		 * \brief Check whether the specified key exists.
		 *
		 * @param name  Name of the key.
		 * @return      {@code true} when the key is present, {@code false} otherwise.
		 */
		bool has_key(const std::string name) const;

		/**
		 * \brief Convert leJOS port name to ev3dev port address.
		 *
		 * @param lejosName   leJOS name.
		 * @return            ev3dev name.
		 */
		static ev3dev::address_type port_java2cpp(std::string lejosName);

	private:
		std::unordered_map<std::string, std::string> *p_values; ///< backing store
	};

}
#endif //ROBOFAT_SETTINGS_H
