//
// Created by kuba on 11.6.16.
//

#ifndef PROGRAM_CPP_PARSER_H
#define PROGRAM_CPP_PARSER_H

#include <fstream>
#include "path.h"
#include "map.h"

namespace tuxtardis {
	extern void parse_space(std::ifstream &input, tuxtardis::map &map, tuxtardis::path &path);
}


#endif //PROGRAM_CPP_PARSER_H
