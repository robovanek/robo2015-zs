//
// Created by kuba on 11.6.16.
//

#include "parser.h"
#include <sstream>
#include <algorithm>

using namespace std;
const vector<int> WALL_VECTOR = {-1};

vector<int> get_numbers(const string &s) {
	vector<int> elems;
	istringstream stream(s);
	string item;
	while (getline(stream, item, '+')) {
		elems.push_back(stoi(item));
	}
	return elems;
}

void tuxtardis::parse_space(std::ifstream &input, tuxtardis::map &map, tuxtardis::path &path) {
	vector<string> elems;
	string item;
	while (getline(input, item, ',')) {
		item.erase(remove_if(item.begin(), item.end(), ::isspace), item.end());
		transform(item.begin(), item.end(), item.begin(), ::tolower);
		elems.push_back(item);
	}

	vector<int> array[map::size];
	int elems_size = elems.size();
	for (int i = 0; i < map::size && i < elems_size; i++) {
		string elem = elems[i];
		if (elem == "x") {
			map[i] = tile_wall;
			array[i] = WALL_VECTOR;
		} else {
			map[i] = tile_lon;
			vector<int> nums = get_numbers(elems[i]);
			array[i].swap(nums);
		}
	}

	point current = map::center;
	int index = 0;
	while (current != point_invalid) {
		point next = point_invalid;
		vector<point> adjacent = next.adjacent();
		for (point p : adjacent) {
			vector<int> &vec = array[map::index(p)];
			if (std::find(vec.begin(), vec.end(), index + 1) != vec.end()) {
				path.push(p);
				next = p;
				break;
			}
		}
		index++;
		current = next;
	}
}

