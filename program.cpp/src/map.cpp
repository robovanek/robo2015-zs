//
// Created by kuba on 10.6.16.
//

#include <cstring>
#include <cmath>
#include "map.h"

const tuxtardis::point tuxtardis::map::center = point(5, 4);

tuxtardis::map::map() {
	stor = new tile[size];
	tile *end = stor + size;
	for (tile *ptr = stor; ptr < end; ptr++) {
		*ptr = tile_lon;
	}
	for (int x = 0; x < width; x++) {
		stor[index(x, 0)] = tile_wall;
		stor[index(x, height - 1)] = tile_wall;
	}
	for (int y = 1; y < height - 1; y++) {
		stor[index(0, y)] = tile_wall;
		stor[index(width - 1, y)] = tile_wall;
	}
	stor[index(center.x - 1, center.y)] = tile_wall;
	stor[index(center.x, center.y)] = tile_wall;
	stor[index(center.x + 1, center.y)] = tile_wall;
}

tuxtardis::map::map(tile data[width][height]) {
	stor = new tile[width * height];
	for (int x = 0; x < width; x++) {
		tile *ptr = data[x];
		for (int y = 0; y < height; y++) {
			stor[index(x, y)] = ptr[y];
		}
	}
}

tuxtardis::map::~map() {
	delete[] stor;
}

tuxtardis::tile &tuxtardis::map::operator[](const point &p) {
	return stor[index(p)];
}


tile &map::operator[](int i) {
	if (i < 0 || i >= size)
		throw out_of_bounds(size / width, size % width, "direct access error");
	return stor[i];
}

tuxtardis::tile tuxtardis::map::get(int x, int y) {
	return stor[index(x, y)];
}

tuxtardis::tile tuxtardis::map::get(const point &p) {
	return stor[index(p)];
}

void tuxtardis::map::set(int x, int y, tile t) {
	stor[index(x, y)] = t;
}

void tuxtardis::map::set(const point &p, tile t) {
	stor[index(p)] = t;
}

void tuxtardis::map::move(int count) {
	int signum = (count > 0) - (count < 0);
	for (int i = 0; i < std::abs(count); i++) {
		tile &tile = stor[index(pos_current)];
		if (tile == tile_lon)
			tile = tile_loff;
		pos_current.move(dir_current, signum);
	}
}


out_of_bounds::out_of_bounds()
		: p(point_invalid) { }

out_of_bounds::out_of_bounds(int x, int y)
		: p(x, y) { }

out_of_bounds::out_of_bounds(int x, int y, std::string msg)
		: p(x, y), msg(msg) { }

out_of_bounds::out_of_bounds(point p)
		: p(p) { }

out_of_bounds::out_of_bounds(point p, std::string msg)
		: p(p), msg(msg) { }









