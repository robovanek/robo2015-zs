//
// Created by kuba on 10.6.16.
//

#ifndef PROGRAM_CPP_POINT_H
#define PROGRAM_CPP_POINT_H


#include <stdint-gcc.h>
#include <vector>

namespace tuxtardis {
	/**
	 * \brief Direction on the playing surface.
	 *
	 * @author Jakub Vaněk <vanek.jakub4@seznam.cz>
	 */
	enum direction : int {
		/**
		 * \brief Error.
		 * Used only as return value, don't use as an argument.
		 */
				dir_error = INT32_MIN,
		/**
		 * \brief North, forwards from the start.
		 */
				dir_north = 0,
		/**
		 * \brief East, from the start to the right.
		 */
				dir_east = 1,
		/**
		 * \brief South, backwards from the start.
		 */
				dir_south = 2,
		/**
		 * \brief West, from the start to the left.
		 */
				dir_west = 3,
		/**
		 * \brief Number of directions.
		 */
				dir_count = 4
	};

	/**
	 * \brief Calculate difference between two directions.
	 *
	 * @param orig      Reference direction.
	 * @param newdir    New direction.
	 * @return          Difference between these two.
	 */
	inline int operator-(direction newdir, direction orig) {
		return (int) newdir - (int) orig;
	}

	/**
	 * \brief Rotate the direction by a specified number of 90° turns CW.
	 *
	 * @param dir   Starting direction.
	 * @param edit  How many times it's going to turn 90° to the right.
	 * @return      Rotated direction.
	 */
	inline direction operator+(direction dir, int edit) {
		return (direction) ((((int) dir) + dir_count + (edit % dir_count)) % dir_count);
	}

	/**
	 * \brief Rotate the direction by a specified number of 90° turns CW.
	 *
	 * @param a     Starting direction.
	 * @param edit  How many times it's going to turn 90° to the right.
	 * @return      Rotated direction.
	 */
	inline direction &operator+=(direction &a, int edit) {
		return a = a + edit;
	}

	/**
	 * \brief Rotate the direction by a specified number of 90° turns CCW.
	 *
	 * @param dir   Starting direction.
	 * @param edit  How many times it's going to turn 90° to the left.
	 * @return      Rotated direction.
	 */
	inline direction operator-(direction dir, int edit) {
		return (direction) ((((int) dir) + dir_count - (edit % dir_count)) % dir_count);
	}

	/**
	 * \brief Rotate the direction by a specified number of 90° turns CCW.
	 *
	 * @param a     Starting direction.
	 * @param edit  How many times it's going to turn 90° to the left.
	 * @return      Rotated direction.
	 */
	inline direction &operator-=(direction &a, int edit) {
		return a = a - edit;
	}

	/**
	 * \brief Ordered pair of <x,y> coordinates.
	 *
	 * @author Jakub Vaněk <vanek.jakub4@seznam.cz>
	 */
	struct point {
	public:
		/**
		 * \brief Trivial constructor.
		 */
		point();

		/**
		 * \brief Construct new point.
		 *
		 * @param x     X coordinate.
		 * @param y     Y coordinate.
		 */
		point(int x, int y);

		/**
		 * \brief Construct new point.
		 *
		 * @param arr   X and Y coordinates.
		 */
		point(const int arr[2]);

		/**
		 * \brief Move this point in the specified direction by the specified distance.
		 *
		 * @param dir   Direction to move in.
		 * @param dist  Distance to move by.
		 */
		void move(direction dir, int dist);

		/**
		 * \brief Get adjacent points of this point.
		 *
		 * @return      List of adjacent points.
		 */
		std::vector<point> adjacent() const;

		/**
		 * \brief Get adjacent points of this point optimized to current direction.
		 *
		 * @return      List of adjacent points with optimal order.
		 */
		std::vector<point> adjacent(direction fwd) const;

		/**
		 * \brief Get the direction of a point sharing one common coordinate.
		 *
		 * @param adjacent  Point with one common coordinate.
		 * @return          Direction of point if it has one common coordinate and it's not the same point. dir_error is returned otherwise.
		 */
		direction direction_of_adjacent(const point &adjacent) const;

		/**
		 * \brief Copy and move the specified point in the specified direction by the specified distance.
		 *
		 * @param orig  Point to move.
		 * @param dir   Direction to move in.
		 * @param dist  Distance to move by.
		 * @return      Moved point.
		 */
		static point move_copy(point orig, direction dir, int dist);

		/**
		 * \brief Check whether that point is equal to this point.
		 *
		 * @param test  Tested point
		 * @return      True if it's the same coordinates, false otherwise.
		 */
		bool operator==(const point &test) const {
			return this->x == test.x && this->y == test.y;
		}

		/**
		 * \brief Check whether that point is equal to this point.
		 *
		 * @param test  Tested point
		 * @return      True if it's the same coordinates, false otherwise.
		 */
		bool operator==(const int arr[2]) const {
			return this->x == *(arr++) && this->y == *arr;
		}

		/**
		 * \brief Check whether that point is equal to this point.
		 *
		 * @param test  Tested point
		 * @return      True if it's the same coordinates, false otherwise.
		 */
		bool operator!=(const point &test) const {
			return this->x != test.x || this->y != test.y;
		}


		/**
		 * \brief Check whether that point is equal to this point.
		 *
		 * @param test  Tested point
		 * @return      True if it's the same coordinates, false otherwise.
		 */
		bool operator!=(const int arr[2]) const {
			return this->x != *(arr++) || this->y != *arr;
		}

		/**
		 * \brief Assignment operator.
		 *
		 * @param b     What to assign from.
		 * @return      This point.
		 */
		point &operator=(const point &b) {
			this->x = b.x;
			this->y = b.y;
			return *this;
		}

		/**
		 * \brief Assignment operator.
		 *
		 * @param arr   What to assign from.
		 * @return      This point.
		 */
		point &operator=(const int arr[2]) {
			this->x = *(arr++);
			this->y = *arr;
			return *this;
		}

		/**
		 * \brief X coordinate.
		 */
		int x;
		/**
		 * \brief Y coordinate.
		 */
		int y;
	};

	/**
	 * \brief Invalid coordinate.
	 */
	const point point_invalid = tuxtardis::point(INT32_MIN, INT32_MIN);
}

#endif //PROGRAM_CPP_POINT_H
