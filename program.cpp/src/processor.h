//
// Created by kuba on 11.6.16.
//

#ifndef PROGRAM_CPP_PROCESSOR_H
#define PROGRAM_CPP_PROCESSOR_H

#include <step_pilot.h>
#include "map.h"
#include "path.h"

using robofat::step_pilot;
namespace tuxtardis {
	enum turn_mode {
		turn_only,
		turn_step,
		turn_speed
	};

	class processor {

	public:
		processor(step_pilot &hw, tuxtardis::map &map, tuxtardis::path &path);

		void follow();

		processor &set_tile_size(int size);

		processor &set_travelback(int dist);

		processor &set_max_turn_error(int angle);

		processor &set_turning_mode(turn_mode mode);

		processor &set_caching(bool on);

		processor &set_align(bool on);

	private:
		enum op {
			op_forward, op_backward, op_left, op_right
		};

		void forward();

		void backward();

		void right();

		void left();

		void flush();

		void process();

		bool check_flush();

		int count_ops();

		bool align_backtrace();

		void rotate(int turn);

		robofat::step_pilot &hardware;
		tuxtardis::map &map;
		tuxtardis::path &path;
		std::deque<op> cache;
		bool do_caching = true;
		bool do_align = true;
		int tile = 280;
		int maxerror = 10;
		int travelback = 0;
		bool travelback_pending = false;
		turn_mode rotate_mode = turn_step;
	};
}

#endif //PROGRAM_CPP_PROCESSOR_H
