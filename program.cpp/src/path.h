//
// Created by kuba on 11.6.16.
//

#ifndef PROGRAM_CPP_PATH_H
#define PROGRAM_CPP_PATH_H


#include <queue>
#include "point.h"
#include <fstream>

namespace tuxtardis {
	typedef std::queue<point> path;
}

#endif //PROGRAM_CPP_PATH_H
