//
// Created by kuba on 10.6.16.
//

#include <vector>
#include "point.h"

tuxtardis::point::point(int x, int y)
		: x(x), y(y) {
}

void tuxtardis::point::move(direction dir, int distance) {
	switch (dir) {
		case dir_north:
			y -= distance;
			break;
		case dir_east:
			x += distance;
			break;
		case dir_south:
			y += distance;
			break;
		case dir_west:
			x -= distance;
			break;
		default:
			break;
	}
}

std::vector<tuxtardis::point> tuxtardis::point::adjacent() const {
	return {point(x, y - 1),
	        point(x + 1, y),
	        point(x, y + 1),
	        point(x - 1, y)};
}

std::vector<tuxtardis::point> tuxtardis::point::adjacent(direction fwd) const {
	direction perpendicular = fwd + 1;
	return {move_copy(*this, fwd, 1),
	        move_copy(*this, perpendicular, 1),
	        move_copy(*this, perpendicular, -1),
	        move_copy(*this, fwd, -1)};
}

tuxtardis::direction tuxtardis::point::direction_of_adjacent(const point &adjacent) const {
	if ((this->x != adjacent.x && this->y != adjacent.y) ||
	    (this->x == adjacent.x && this->y == adjacent.y))
		return dir_error;
	if (this->x == adjacent.x) {
		if (this->y > adjacent.y) {
			return dir_north;
		} else {
			return dir_south;
		}
	} else {
		if (this->x > adjacent.x) {
			return dir_east;
		} else {
			return dir_west;
		}
	}
}

tuxtardis::point tuxtardis::point::move_copy(point orig, direction dir, int distance) {
	orig.move(dir, distance);
	return orig;
}

tuxtardis::point::point(const int *arr) {
	this->x = *(arr++);
	this->y = *arr;
}

tuxtardis::point::point() : x(0), y(0) {
}



