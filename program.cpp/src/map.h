//
// Created by kuba on 10.6.16.
//

#ifndef PROGRAM_CPP_MAP_H
#define PROGRAM_CPP_MAP_H

#include <string>
#include <fstream>
#include "point.h"

using namespace tuxtardis;

namespace tuxtardis {
	enum tile : int {
		tile_lon,
		tile_loff,
		tile_wall
	};

	class out_of_bounds {
	public:
		const point p;
		const std::string msg;

		out_of_bounds();

		out_of_bounds(int x, int y);

		out_of_bounds(int x, int y, std::string msg);

		out_of_bounds(point p);

		out_of_bounds(point p, std::string msg);
	};

	class map {
	public:
		const static int width = 11;
		const static int height = 8;
		const static size_t size = width * height;
		const static point center;
		direction dir_current = dir_north;
		point pos_current = center;

		map();

		map(tile data[width][height]);

		~map();

		tile get(int x, int y);

		tile get(const point &p);

		void set(int x, int y, tile t);

		void set(const point &p, tile t);

		tile &operator[](const point &p);

		tile &operator[](int i);

		inline void turn(int count) {
			dir_current += count;
		}

		inline void turn(direction d) {
			dir_current = d;
		}

		inline static int index(const point &p) {
			return index(p.x, p.y);
		}

		inline static int index(int x, int y) {
			if (x < 0 || x >= width || y < 0 || y >= height)
				throw out_of_bounds(x, y);
			return y * width + x;
		}

		void move(int count);

	private:
		tile *stor;
	};
}

#endif //PROGRAM_CPP_MAP_H
