//
// Created by kuba on 11.6.16.
//

#include "processor.h"

processor::processor(step_pilot &hw, tuxtardis::map &map, tuxtardis::path &path)
		: hardware(hw), map(map), path(path), cache() {
}

void processor::follow() {
	while (!path.empty() && !ev3dev::button::back.pressed()) {
		point &p = path.front();
		if (p == map.pos_current)
			continue;
		direction dir = map.pos_current.direction_of_adjacent(p);
		int diff = dir - map.dir_current;
		switch (diff) {
			default:
			case 0:
				forward();
				break;
			case 2:
			case -2:
				backward();
				break;
			case 1:
				right();
				forward();
				break;
			case -1:
				left();
				forward();
				break;
		}
	}
	if (!ev3dev::button::back.pressed())
		flush();
	else
		hardware.stop(false);
}

void processor::forward() {
	cache.push_back(op_forward);
	map.move(1);
	process();
}

void processor::backward() {
	cache.push_back(op_backward);
	map.move(-1);
	process();
}

void processor::right() {
	cache.push_back(op_right);
	map.turn(1);
	process();
}

void processor::left() {
	cache.push_back(op_left);
	map.turn(-1);
	process();
}

void processor::process() {
	bool flush_needed = check_flush() || !do_caching;
	if (flush_needed)
		flush();
}

void processor::flush() {
	while (!cache.empty()) {
		op cmd = cache.front();
		int count = count_ops();
		switch (cmd) {
			case op_forward:
				bool align = do_align && align_backtrace();
				if (align) {
					hardware.move_to_wall();
					travelback_pending = true;
				} else {
					hardware.move(tile * count, true);
				}
				break;
			case op_backward:
				hardware.move(-tile * count - travelback_pending * travelback, true);
				travelback_pending = false;
				break;
			case op_left:
				rotate(count);
				break;
			case op_right:
				rotate(-count);
				break;
		}
	}
}

bool processor::check_flush() {
	return std::any_of(cache.cbegin(), cache.cend(),
	                   [](op o) { return o != op_forward && o != op_backward; });
}

int processor::count_ops() {
	op orig = cache.front();
	op next;
	int count = 0;
	do {
		count++;
		cache.pop_front();
		if (cache.empty())
			break;
		next = cache.front();
	} while (orig == next);
	return count;
}

bool processor::align_backtrace() {
	std::deque<op> copy = cache;
	point pos_current = map.pos_current;
	direction dir_current = map.dir_current;
	while (!copy.empty()) {
		op o = copy.back();
		copy.pop_back();
		switch (o) {
			case op_forward:
				pos_current.move(dir_current, -1);
				break;
			case op_backward:
				pos_current.move(dir_current, 1);
				break;
			case op_left:
				dir_current += 1;
				break;
			case op_right:
				dir_current -= 1;
				break;
		}
	}
	pos_current.move(dir_current, 1);
	return map[pos_current] == tile_wall;
}

void processor::rotate(int count) {
	if (travelback_pending) {
		hardware.move(-travelback, true);
		travelback_pending = false;
	}
	int turn = 90 * count;
	switch (rotate_mode) {
		case turn_only:
			hardware.turn_only(turn, true);
			break;
		case turn_step:
			hardware.turn_p_step(turn, maxerror);
			break;
		case turn_speed:
			hardware.turn_pid_linear(turn, maxerror);
			break;
	}
}


processor &processor::set_tile_size(int size) {
	tile = size;
	return *this;
}

processor &processor::set_travelback(int distance) {
	travelback = distance;
	return *this;
}

processor &processor::set_max_turn_error(int angle) {
	maxerror = angle;
	return *this;
}

processor &processor::set_turning_mode(turn_mode mode) {
	rotate_mode = mode;
	return *this;
}

processor &processor::set_caching(bool on) {
	do_caching = on;
	return *this;
}

processor &processor::set_align(bool on) {
	do_align = on;
	return *this;
}