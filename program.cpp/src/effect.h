//
// Created by kuba on 11.6.16.
//

#ifndef PROGRAM_CPP_EFFECT_H
#define PROGRAM_CPP_EFFECT_H

#include <future>
#include <ev3dev.h>
#include "config.h"

namespace tuxtardis {
	void init_tune() {
		ev3dev::sound::play(TUNE_PATH, true);
	}

	void danger_beep() {
		ev3dev::sound::tone(800.0f, 500.0f, true);
	}

	void two_beep_async() {
		static std::vector<std::vector<float>> data = {{600.0f, 200.0f, 50.0f},
		                                               {600.0f, 200.0f}};
		ev3dev::sound::tone(data, false);
	}
}
#endif //PROGRAM_CPP_EFFECT_H
