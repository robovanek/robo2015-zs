#include <iostream>
#include <led.h>
#include <settings.h>
#include <display.h>
#include "tuxtardis.h"
#include "config.h"
#include "path.h"
#include <sstream>
#include <step_pilot.h>
#include "parser.h"
#include "processor.h"
#include "effect.h"
#include <util.h>

using namespace std;
using namespace robofat;
using namespace robofat::utility;
using namespace tuxtardis;

settings config;
display screen;

string get_map_name() {
	int maps = config.get_integer("maps");
	vector<string> opts;
	for (int i = 1; i <= maps; i++) {
		opts.push_back("Map #" + i);
	}
	int selected = screen.show_menu("Select map:", opts, 0, false) + 1;
	ostringstream result;
	result << MAP_DIR;
	result << selected;
	result << ".map";
	return result.str();
}

void load_conf() {
	ifstream f(CONFIG_PATH);
	config.load(f, true, true);
	f.close();
}

turn_mode getmode() {
	string value = config.get("turn regulation");
	if (value == "off")
		return turn_only;
	if (value == "step")
		return turn_step;
	if (value == "linear")
		return turn_speed;
	return turn_only;
}

bool ask() {
	static vector<string> opts = {"START", "exit"};
	int selected = screen.show_menu("Ready!", opts, 0, false);
	return (bool) (1 - selected);
}

void random_fuck(step_pilot &pilot) {
	std::srand((unsigned int) time(NULL));
	while (!ev3dev::button::back.pressed()) {
		int rand = ftoi(std::rand()) % 4;
		switch(rand){
			// todo random action
		}
	}
}

int main() {
	led_set(RED, RED);
	screen.print("loading...");
	load_conf();

	led_set(GREEN, GREEN);
	string map = get_map_name();

	led_set(ORANGE, ORANGE);
	screen.init_begin("Hardware init");
	auto lmotor = config.get_port("");
	auto rmotor = config.get_port("");
	auto touch = config.get_port("");
	auto gyro = config.get_port("");
	auto trackwidth = config.get_float("");
	auto wheeldiameter = config.get_float("");
	step_pilot pilot(lmotor, rmotor, &touch, &gyro, trackwidth, wheeldiameter, &screen);
	//fixme p.set... BLAH BLAH BLAH
	screen.init_end(true);

	led_set(RED, RED);
	screen.init_begin("Gyro calibrate");
	danger_beep();
	pilot.gyro_calibrate();
	screen.init_end(true);

	led_set(ORANGE, ORANGE);
	screen.init_begin("Load map & path");
	map m;
	path p;
	ifstream f(map);
	parse_space(f, m, p);
	f.close();
	screen.init_end(true);
	screen.init_begin("Processing init");
	processor proc(pilot, m, p);
	proc.set_align(config.get_bool(""))
			.set_caching(config.get_bool(""))
			.set_max_turn_error(config.get_integer(""))
			.set_tile_size(config.get_integer(""))
			.set_travelback(config.get_integer(""))
			.set_turning_mode(getmode());
	screen.init_end(true);

	led_set(GREEN, GREEN);
	two_beep_async();
	if (!ask())
		return 1;
	screen.init_log("Starting!");
	screen.clear_display();
	screen.print_centered("Tuxova TARDIS", robofat::display::attrib::attr_bold);
	screen.print_centered("ev3dev/C++", robofat::display::attrib::attr_normal);
	proc.follow();

	random_fuck(pilot);

	return 0;
}