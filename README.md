# Tuxova TARDIS - repozitář (nejen) programu na Robosoutěž ZŠ 2015
## Programování
Program je v Javě [(LeJOS)](http://www.robosoutez.cz/index.php?sekce=home&id=nxj). Máme EV3 sadu, tudíž jsme použili [LeJOS EV3](http://www.lejos.org/ev3.php).

Jako *"základový"* program jsme si vzali toto: [Advanced Robotics - Robosoutěž SŠ 2014](https://bitbucket.org/JakubVanek/robosoutez2014). Přístup dám na požádání.
### Styl kódu
Použíté styly:

- K&R
- Allman (snad už nikde)

### Čeština
Komentáře by měly být česky s diakritikou a měly by nějak rozvíjet to, k čemu se vztahují. Displej EV3 neumí zobrazit diakritiku, tudíž vypisovat můžeme pouze angličtinu nebo češtinu bez diakritiky. Commitové zprávy mohou být s diakritikou, ale jména větví (branch) už ne.

### Program

Program funguje tak, že na začátku načte konfiguraci z textového souboru *tux.conf*. Pak se robot na displeji zeptá, na jakém rozložení mapy se nachází a jak má jet. To načte a pak postupně inicializuje jednotlivé komponenty a počká na spuštění. Jízda probíhá tak, že robot postupně odebírá z naplánované trasy jednotlivé body a podle jejich odklonění od současného směru je přidá do cache buď jako příkaz pro jízdu dopředu, dozadu, doleva nebo doprava. Cache funguje tak, že v sobě má frontu příkazů, kterou udržuje do té doby, kdy jsou ve frontě příkazy jen dopředu/dozadu. Pokud se tam vyskytne zatočení, cache postupně spočítá bloky příkazů a vykoná je najednou. To znamená, že příkaz dopředu-dopředu-doleva-dozadu-dozadu se při vykonání cache vykoná jako 2x délka dopředu, doleva; a pak při dalším vykonání jako 2x délka dozadu. Když tento primární program skončí, nastupuje náhodné ježdění v bludišti.

Zajímavost o zatáčení, na kterou se mě pár lidí ptalo: otáčení korigujeme gyroskopem a proporcionálním regulátorem (viz [prezentace ze stránek Robosoutěže](http://www.robosoutez.cz/files/Regulatory2.pdf)). Nejdříve se robot otočí přes přepočet na kola o zadaný počet stupňů. Poté koriguje odchylku získanou z gyroskopu tak dlouho, dokud není v minimální odchylce. Překmitávání na soutěži bylo způsobené moc velkou proporcionální konstantou.

** *Z minulého programu byly přejaty akorát podpůrné třídy - mapa, pozice, typ pole, orientace robota, nápad na Mechanics a nápad na Path.* **

## Členové týmu
Všichni z O3 (rok 2014/2015) :)

- Jakub Vaněk
- Jakub Švorc
- Michal Nekvinda
## Robot
Návrhy na 4 typy:

- věž - *nepodařilo se postavit*
- **2.5 kolka - *postavena, bude (byla) použita na soutěži* **
- autíčko - *neumělo zatáčet*
- tank - *třásl se*

## Odkazy
### Soutěž
- Robosoutěž ZŠ: http://www.robosoutez.cz/index.php?sekce=robosoutezprozs&id=robosoutezprozs_1415
- Zadání: http://www.robosoutez.cz/index.php?sekce=robosoutezprozs&id=robosoutezprozs_1415_rules
- FAQ: http://www.robosoutez.cz/index.php?sekce=robosoutezprozs&id=robosoutezprozs_1415_question_and_answers
- Výsledky: http://www.robosoutez.cz/index.php?sekce=robosoutezprozs&id=robosoutezprozs_1415_order
### Dokumentace
- Program pro pohodlnou práci s GITem: https://www.atlassian.com/software/sourcetree/overview
- Tutorial na GIT: http://git-scm.com/book/cs/v1
### Vývojářské nástroje
- LDD: http://ldd.lego.com/cs-cz/
- LeJOS: [Hlavní stránka](http://www.lejos.org/), [EV3](http://www.lejos.org/ev3.php)

IDE:

- Eclipse: https://eclipse.org/
- Eclipse plugin: [EV3](http://sourceforge.net/p/lejos/wiki/Installing%20the%20Eclipse%20plugin/)
- IntelliJ IDEA: https://www.jetbrains.com/idea/

## Další týmy z naší školy

- Tým gQsoQa - Karel Vítek, Matěj Kripner a Vojtěch Svoboda; všichni z O4 * // report: tak vzhledem k jejich 36. místu ze 36 týmů a 2 z 80 bodů z první fáze soutěže udělali takový větší [FAILED] :D *

### P.S.:
Musíme porazit kvartu :D * // report: oni se zlikvidovali sami :D *